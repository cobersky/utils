package uuid

import (
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/sony/sonyflake"
	"strconv"
)

var (
	_generator = sonyflake.NewSonyflake(sonyflake.Settings{})
)

func SetMachineId(mid uint16) {
	_generator = sonyflake.NewSonyflake(sonyflake.Settings{MachineID: func() (u uint16, e error) {
		return mid, nil
	}})
}

type UUID int64

func (id UUID) Hex() string {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(id))
	return hex.EncodeToString(b)
}
func (id UUID) String() string {
	return strconv.FormatInt(int64(id),10)
}
func ParseHex(str string) (UUID, error) {
	b, err := hex.DecodeString(str)
	if err != nil {
		return 0, err
	}
	return UUID(int64(binary.BigEndian.Uint64(b))), nil
}

func (id UUID) MarshalJSON() ([]byte, error) {
	return []byte(`"` + strconv.FormatInt(int64(id), 10) + `"`), nil
}

func (id *UUID) UnmarshalJSON(b []byte) error {
	if len(b) < 2 {
		return errors.New("invalid uuid input")
	} else if string(b) == `""` {
		*id = UUID(0)
		return nil
	}
	n, err := strconv.ParseInt(string(b[1:len(b)-1]), 10, 64)
	if err != nil {
		*id = UUID(0)
		fmt.Println("invalid uuid:", string(b))
	} else {
		*id = UUID(n)
	}
	return nil
}

func New() UUID {
	id, _ := _generator.NextID()
	return UUID(id)
}
