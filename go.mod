module gitee.com/cobersky/utils

go 1.13

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.170
	github.com/aliyun/aliyun-oss-go-sdk v2.1.0+incompatible
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sony/sonyflake v1.0.0
	xorm.io/xorm v1.0.1
)
