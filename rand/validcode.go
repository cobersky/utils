package rand

import (
	"bytes"
	"math/rand"
	"strconv"
	"time"
)
var _r = rand.New(rand.NewSource(time.Now().Unix()))

func ValidCode(n int) string {
	s := &bytes.Buffer{}
	for i := 0; i < n; i++ {
		s.WriteString(strconv.Itoa(_r.Intn(10)))
	}
	return s.String()
}
