package oss

import (
	"bytes"
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"log"
)

type Client struct {
	ossCli *oss.Client
}

func NewClient(endpoint, accessKeyId, accessSecret string) (*Client, error) {
	client, err := oss.New(endpoint, accessKeyId, accessSecret)
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	return &Client{ossCli: client}, nil
}
func NewCli(conf *Config) (*Client, error) {
	client, err := oss.New(conf.EndPoint, conf.AccessKey, conf.AccessSecret)
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	return &Client{ossCli: client}, nil
}

type Config struct {
	EndPoint     string
	AccessKey    string
	AccessSecret string
}

func Handle(conf *Config, bucketName , pathParamKey string) (func(ctx *gin.Context), error) {
	client, err :=oss.New(conf.EndPoint, conf.AccessKey, conf.AccessSecret)

	if err != nil {
		return nil, err
	}
	bucket,err:=client.Bucket(bucketName)
	if err != nil {
		return nil,err
	}

	return func(ctx *gin.Context) {
		objectKey :=ctx.Param(pathParamKey)
		if len(objectKey)==0{
			log.Println("object key is empty!")
			ctx.AbortWithStatus(404)
			return
		}
		if objectKey[0]=='/'{
			objectKey=objectKey[1:]
		}
		log.Println("key:",objectKey)
		reader, err := bucket.GetObject(objectKey)
		if err != nil {
			log.Println(err)
			ctx.AbortWithStatus(404)
			return
		}
		b, err := ioutil.ReadAll(reader)
		if err != nil {
			log.Println(err)
			ctx.AbortWithStatus(404)
			return
		}
		ctx.Status(200)
		ctx.Writer.Write(b)
	}, nil
}
func (cli *Client) MoveFile(bucketName, from, to string) error {
	bucket, err := cli.ossCli.Bucket(bucketName)
	if err != nil {
		fmt.Println("Error:", err)
		return err
	}
	_, err = bucket.CopyObject(from, to)
	if err != nil {
		fmt.Println("Error2:", err, bucketName, from, to)
		return err
	}

	return nil
}
func (cli *Client) AddFile2(bucketName, name string, data []byte) error {
	return cli.AddFile(bucketName, name, bytes.NewReader(data))
}
func (cli *Client) AddFile(bucketName, name string, reader io.Reader) error {
	bucket, err := cli.ossCli.Bucket(bucketName)
	if err != nil {
		return err
	}
	err = bucket.PutObject(name, reader)
	if err != nil {
		return err
	}
	return nil
}
func (cli *Client) DeleteFile(bucketName, name string) error {

	bucket, err := cli.ossCli.Bucket(bucketName)
	if err != nil {
		fmt.Println("Error:", err)
		return err
	}

	err = bucket.DeleteObject(name)
	return err
}

func (cli *Client) GetFile(bucketName, name string) ([]byte, error) {
	bucket, err := cli.ossCli.Bucket(bucketName)
	if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}

	reader, err := bucket.GetObject(name)
	if err != nil {
		return nil, err
	}
	b, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (cli *Client) Exists(bucketName, name string) (bool, error) {
	bucket, err := cli.ossCli.Bucket(bucketName)
	if err != nil {
		fmt.Println("Error:", err)
		return false, err
	}
	return bucket.IsObjectExist(name)
}
