package sms

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/requests"
)

type respStruct struct {
	Message   string //"OK"
	RequestId string //"ADAFB34D-E6AA-4D26-B8C4-E32190ADD39C"
	BizId     string //"521600352662901096^0"
	Code      string //"OK"
}

func Send(phoneNumber, code string,templateCode,accessKeyId,accessSecret,signName string) error {
	client, err := sdk.NewClientWithAccessKey("default", accessKeyId, accessSecret)
	if err != nil {
		return err
	}
	request := requests.NewCommonRequest()
	request.Method = "POST"
	request.Scheme = "https" // https | http
	request.Domain = "dysmsapi.aliyuncs.com"
	request.Version = "2017-05-25"
	request.ApiName = "SendSms"
	request.QueryParams["TemplateCode"] = templateCode
	request.QueryParams["PhoneNumbers"] = phoneNumber
	request.QueryParams["SignName"] = signName
	request.QueryParams["TemplateParam"] = fmt.Sprintf(`{"code":"%s"}`, code)

	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		return err
	}
	var resp respStruct
	err = json.Unmarshal(response.GetHttpContentBytes(), &resp)
	if err != nil {
		return err
	}
	if resp.Code != "OK" {
		return errors.New("response error: " + resp.Code + " , BizId: " + resp.BizId)
	}

	return nil
}

