package xormutil

import (
	"errors"
	"gitee.com/cobersky/utils/uuid"
	"xorm.io/xorm"
)

type IId interface {
	SetId(uuid uuid.UUID)
	GetId() uuid.UUID
}

type SQLAction interface {
	Invoke(sess *xorm.Session) error
}
type createAction struct {
	bean interface{}
}

func (a *createAction) Invoke(sess *xorm.Session) error {
	if aa, ok := a.bean.(IId); ok {
		if aa.GetId() == 0 {
			aa.SetId(uuid.New())
		}
	}
	_, err := sess.InsertOne(a.bean)
	if err != nil {
		return err
	}
	return nil
}

func NewCreateAction(bean interface{}) SQLAction {
	return &createAction{
		bean: bean,
	}
}
type deleteAction struct {
	bean interface{}
}
func (a *deleteAction) Invoke(sess *xorm.Session) error {
	n, err := sess.Delete(a.bean)
	if err != nil {
		return err
	}
	if n==0{

	}
	return nil
}
func NewDeleteAction(bean interface{}) SQLAction {
	return &deleteAction{
		bean: bean,
	}
}
type updateAction struct {
	id   interface{}
	bean interface{}
	cols []string
}

func (a *updateAction) Invoke(sess *xorm.Session) error {
	n,err:=sess.ID(a.id).Cols(a.cols...).Update(a.bean)
	if err != nil {
		return err
	}
	if n==0{
		return errors.New("system busy")
	}
	return nil
}

func NewUpdateAction(id interface{}, bean interface{}, cols ...string) SQLAction {
	return &updateAction{id: id, bean: bean, cols: cols}
}
func ExecuteAll(engine *xorm.Engine, actions ...SQLAction) error {
	if len(actions) == 0 {
		return nil
	}
	sess := engine.NewSession()
	err := sess.Begin()
	if err != nil {
		return err
	}

	for i := 0; i < len(actions); i++ {
		err = actions[i].Invoke(sess)
		if err != nil {
			_ = sess.Rollback()
			return err
		}
	}
	return sess.Commit()
}

