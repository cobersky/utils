package cloud

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)
type Code int

const (
	CodeSuccess       Code = 200 //调用成功
	CodeError         Code = 400 //参数格式错误
	CodeNoPermission  Code = 403 //无权限调用
	CodeInvalidParams Code = 404 //非法参数
	CodeServerError   Code = 500 //内部服务错误
)

type BizCode int

const (
	BizCodeSuccess    BizCode = 0     //验证通过，即code=200且bizCode=0时为验证通过，其他见具体错误说明
	BizCodeMismatch   BizCode = 13053 //姓名证件号校验不匹配
	BizCodeInaccurate BizCode = 13057 //无法准确校验
	BizCodeMismatch1  BizCode = 13066 //验证不一致
	BizCodeUnknown    BizCode = 13067 //不匹配，但是具体不匹配要素未知
)

type Indetify2Resp struct {
	Code    Code   `json:"code"`
	Message string `json:"message"`
	Value   struct {
		BizCode BizCode `json:"bizCode"`
		Message string  `json:"message"`
	} `json:"value"`
}

func Identify2(idNum string, name string,appCode,userId,verifyKey string) (*Indetify2Resp, error) {
	request, err := http.NewRequest("GET",
		fmt.Sprintf("http://safrvcert.market.alicloudapi.com/safrv_2meta_id_name/?__userId=%s&identifyNum=%s&userName=%s&verifyKey=%s", userId, idNum, name, verifyKey), nil)
	if err != nil {
		return nil, err
	}
	request.Header = http.Header{}
	request.Header.Set("Authorization", fmt.Sprintf("APPCODE %s",appCode))
	httpResp, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}
	if httpResp.StatusCode != 200 {
		return nil, errors.New("service error")
	}
	body, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		return nil, err
	}
	var v Indetify2Resp
	err = json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}
	return &v, nil
}
