package timezone

import "time"

var Beijing = time.FixedZone("Asia/Shanghai", 8*60*60)