package utils

import (
	"errors"
	"strconv"
)

type Identify struct {
	Sex       int8
	BirthYear int
}
func ParseIdentifyNum(num string) (*Identify, error) {
	if len(num) != 18 {
		return nil, errors.New("身份证号码长度错误")
	}
	birth, err := strconv.Atoi(num[6:10])
	if err != nil {
		return nil, err
	}
	sex, err := strconv.Atoi(num[16:17])
	if err != nil {
		return nil, err
	}
	var s int8
	if sex&1 == 0 {
		s = 2
	} else {
		s = 1
	}
	return &Identify{
		Sex:       s,
		BirthYear: birth,
	}, nil
}
