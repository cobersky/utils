package regexp

import "regexp"

var PhoneNumber = regexp.MustCompile(`1[3-9]\d[\-]?\d{4}[\-]?\d{4}`)